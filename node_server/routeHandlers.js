/**
 * Created by WebEntra on 02/05/2018.
 */
var todos = [];
var jsonData = {count: 12, message: 'hey'};

/**
 * middleware
 * middleware are just functions you use for transformation etc before you send the data back
 */
function getRoot(req, res) {
  res.sendFile(__dirname + '/static/index.html', pageErrorHanding, res);
}

function pageErrorHanding(err, res) {
  if (err) {
    res.status(500).send(err);
  }
}

module.exports = {
  getRoot: getRoot,
  pageErrorHanding: pageErrorHanding
}
