/**
 * Created by WebEntra on 28/04/2018.
 */
var http = require('http');
var nodemailer = require('nodemailer');
var formidable = require("formidable");
var util = require('util');
var express = require('express');
var bodyParser = require('body-parser');
var _ = require('lodash');
var app = express();
var routeHandlers = require('./routeHandlers');

app.use(express.static('api'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

/**
 * Routes
 *
 * REST:    CRUD
 * GET = READ
 * POST = WRITE
 * PUT = UPDATE
 * DELETE/DESTROY = DELETE
 *
 */


app.get('/', routeHandlers.getRoot);
const dataStore = [];
const server = http.createServer(function (req, res) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.setHeader('Access-Control-Allow-Methods', 'POST,GET,OPTIONS,PUT,DELETE');

  if (req.method.toLowerCase() == 'post') {
    processForm(req, res);
    return;
  }

  res.end();
});


function processForm(req, res) {
  const form = new formidable.IncomingForm();
  form.parse(req, function (err, fields) {

    res.writeHead(200, {
      'content-type': 'text/plain'
    });

    dataStore.push(fields);
    const data = JSON.stringify(fields);

    res.end(data);

    console.log('posted fields:\n');
    console.log(data);
  });
}

const port = 3100;
server.listen(port);
console.log("server listening on port " + port);
