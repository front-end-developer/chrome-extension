/**
 * Created by Mark Webley on 11/05/2018.
 * Non typescript version
 */

class DataModel {
    constructor(object) {
        this.tab_id = object.tab_id;
        this.url = object.url;
        this.visit_start = object.visit_start;
        this.duration = object.duration;
        this.referrer = object.referrer;
    }
}

class TabAnaliser {
    constructor() {
        this.doAnalysis = 0;
        this.intPassThrough = 0;
        this.getChromeStorage();
        this.startEvents();
    }

    startEvents() {
        const nodeOn = document.querySelector('#on');
        const nodeOff = document.querySelector('#off');
        nodeOn.addEventListener('click', ()=> {
            nodeOn.classList.add('active');
            nodeOff.classList.remove('active');
            this.init();
        });
        nodeOff.addEventListener('click', ()=> {
            nodeOff.classList.add('active');
            nodeOn.classList.remove('active');
            this.disableAnalysis();
        });
    }

    init() {
        const tabData = [];
        let self = this;
        let date = new Date();
        let m = date.getMonth().toString().length === 1 ? '0' + date.getMonth() : date.getMonth();
        let dateToday = date.getDate().toString().length === 1 ? '0' + date.getDate() : date.getDate();
        let vStart;
        let toTS;
        let timespent;

        this.doAnalysis = setInterval(() => {
            chrome.tabs.getSelected(null, (tab) => {
                if (!tabData[tab.id]) {
                    vStart = date.toISOString().replace(/-/g, '');
                    const fixedDuration = new Date().getTime();
                    tabData[tab.id] = {
                        visitStart: vStart,
                        durationStart: fixedDuration,
                        duration: 0
                    };
                    console.log(' Duration ', tabData[tab.id].duration);
                } else {
                    const calcTime = ((new Date().getTime()) - tabData[tab.id].durationStart) / 1000;
                    const diff =  calcTime / 60;
                    tabData[tab.id].duration = Math.abs(Math.round(diff));
                    console.log('total duration: ', tabData[tab.id].duration);
                }
                const model = new DataModel({
                    tab_id: tab.id,
                    url: tab.url,
                    visit_start: tabData[tab.id].visitStart,
                    duration: tabData[tab.id].duration,
                    referrer: document.referrer
                });
                if (this.intPassThrough >= 1) {
                    document.querySelector('.previous_session').classList.add('hide');
                    this.chromeStorage(model);
                }
                this.updateUI(model);
                this.post('http://localhost:3100', model).then(function(response) {
                    console.log('test server responded:', response.statusText);
                });
                this.intPassThrough++;
            });
        },5000);
    }

    disableAnalysis() {
        clearInterval(this.doAnalysis);
    }

    getChromeStorage(){
        // add to/ get from, chrome storage
        chrome.storage.sync.get(['model'], function(result) {
            if (result.model.tab_id) {
                this.updateUIFromPreviousSession(result.model);
            }
        }.bind(this));
    }

    chromeStorage(model) {
        chrome.storage.sync.set({model: model});
        this.chromeNotifications(model);
    }

    chromeNotifications(model) {
        let options = {
            type:   "basic",
            title:  "Tab Analysis",
            message:  "Duration ".concat(model.duration, ' minutes(s) on \n', model.url),
            iconUrl:  "icon.png"
        }
        chrome.notifications.create('duration', options, function() {});
    }

    updateUIFromPreviousSession(model) {
        document.querySelector('.prev_tab_id').innerHTML = model.tab_id;
        document.querySelector('.prev_url').innerHTML = model.url;
        document.querySelector('.prev_duration').innerHTML = model.duration;
    }
    
    updateUI(model) {
        document.querySelector('.tab_id').innerHTML = model.tab_id;
        document.querySelector('.url').innerHTML = model.url;
        document.querySelector('.visit_start').innerHTML = model.visit_start;
        document.querySelector('.duration').innerHTML = model.duration;
        document.querySelector('.referrer').innerHTML = model.referrer;
    }

    post(url, data) {
        console.log('SENDING', JSON.stringify(data));
        return fetch(url, {
            body: JSON.stringify(data),
            cache: 'no-cache',
            credentials: 'omit',
            headers: {
                'Content-Type': 'application/json',
                'Connection': 'keep-alive'
            },
            method: 'POST',
            redirect: 'follow',
            referrer: 'no-referrer'
        });
    }
}

document.addEventListener('DOMContentLoaded', function() {
    new TabAnaliser();
});
