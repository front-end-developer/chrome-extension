/**
 * Created by Mark Webley on 13/05/2018.
 */
class Events {
    constructor() {
        this.init();
    }

    init(){
        chrome.storage.onChanged.addListener(function(changes) {
            chrome.browserAction.setBadgeText({"text": changes.model.newValue.duration.toString()});
        });
    }
}

new Events();