### Tab Analyser Plugin

By Mark Webley:

Track visits of web pages between Tabs. Note that browers change during their lifetime, this plugin is not kept up to date with browser versions, it is an example of my code only.

#### Run:
npm install

cd node_server

npm install




This is just a normal ES6 class version, everything is inside the following folders:
- node_server
- src/dataGenerator/TabAnaliser.js

#### First start node server with:
cd node_server

node server.js


#### add to your local extensions with in

chrome://extensions/

Select "Load Unpacked"

Select the src folder,

enable extension.

If I get more time I can finish the typescript version, but I need to add webpack, configure webpack, and then babel etc...

I left the console logs in there so that you can see what is the code is sending to node.

In node the data the front end sends adds to an node array.


_____
#### Click TabAnalizer button to open plugin options
![Alt text](screenshots/chrome-debugger-icon.png "TabAnalyser Plugin")



#### Plugin disabled:
![Alt text](screenshots/extension-dissabled.png "TabAnalyser Plugin")



#### Plugin enabled:
![Alt text](screenshots/extension-enabled.png "TabAnalyser Plugin")



#### Plugin working with node recieving data
![Alt text](screenshots/screenshot-in-working-localhost.png "TabAnalyser Plugin")



#### Plugin working with node
![Alt text](screenshots/screenshot-new-additions-1.png "TabAnalyser Plugin")



#### Plugin working with node
![Alt text](screenshots/screenshot-new-additions-2.png "TabAnalyser Plugin")





#### Plugin working with node
![Alt text](screenshots/screenshot-new-additions-3.png "TabAnalyser Plugin")

